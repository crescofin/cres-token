# CRES token deployment
The deployment steps are inspired from this post:
https://forum.openzeppelin.com/t/example-using-tokentimelock/2325

## Step 1 - Token deployment
The Token.sol smart contract needs to be compiled and deployed.
This can be done from the Remix IDE:
- for development, the "JavaScript VM" environment can be used
- for test and production, the "Injected Web3" environment can be used. 


Note on the "Injected Web3" environment:

This environment connects to Ethereum via Metamask. Prior to the deployment, Ethereum accounts should be configured in Metamask. Metamask could also be configured to connect to Ethereum nodes managed by SwissLedger. 

When a Token contract instance has been deployed, it is important to record its address.

## Step 2 - TokenGrant deployment
As in the previous step, the TokenGrant.sol smart contract should be deployed.
This contract requires 3 parameters:
- the address of the Token contract instance (from the previous step)
- the address of the beneficiary
- the release time

The release time can be computed using the epock-cli:
```
$ npx epoch-cli "2020/02/26 13:52"
npx: installed 1 in 1.274s
1582685520
```

The TokenGrant contract needs to be instantiated for each beneficiary, and for each vesting period.

## Step 3 - Token transfer
The creator of the Token contract instance initially has control over the total token supply.

A transaction should be initiated for each TokenGrant instance.
The ownership of the tokens will then be transferred to the TokenGrant smart contract until they can be released to the beneficiaries.

## Step 4 - Listing on Uniswap
The listing on Uniswap corresponds to the creation of an ETH/CRES liquidity pool. This liquidity pool initially contains:
- 10,000,000 CRES tokens (10% of the total supply)
- 10,000 CHF in ETH

After the liquidity pool is created, the only possibility for a 3d party to acquire CRES tokens is to trade them against ETH. 

This will cause the cost of CRES tokens against ETH to increase. 
Then the market will find an equilibrium and determine a cost for CRES tokens. 




