pragma solidity ^0.6.0;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/release-v3.1.0/contracts/token/ERC20/ERC20Snapshot.sol";


contract Token is ERC20Snapshot {
    /**
     * We use the ERC20Snapshot contract to facilitate the implementation of voting rights and dividends
     * Total token supply is fixed and equal to 100,000,000
     * Number of decimals is 18 (default value)
     */
    constructor () public ERC20("CrescoFin", "CRES") {
        _mint(msg.sender, 100000000 * (10 ** uint256(decimals())));
    }
}


