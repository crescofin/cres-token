pragma solidity ^0.6.0;

import "github.com/OpenZeppelin/openzeppelin-contracts/blob/release-v3.1.0/contracts/token/ERC20/TokenTimelock.sol";

/**
 * TokenGrant is used to hold the CRES tokens that are granted to the team.
 * For each beneficiary, this contract will be instantiated as many times are required by the vesting schedule
 */
contract TokenGrant is TokenTimelock {
    constructor (address token_address, address beneficiary_address, uint256 releaseTime) public TokenTimelock(
        IERC20(token_address),  
        beneficiary_address, 
        releaseTime) {
    }
}

